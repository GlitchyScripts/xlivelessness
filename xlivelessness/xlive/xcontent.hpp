#pragma once
#include <map>

extern CRITICAL_SECTION xlive_critsec_xcontent;
extern std::map<HANDLE, void*> xlive_xcontent_enumerators;
