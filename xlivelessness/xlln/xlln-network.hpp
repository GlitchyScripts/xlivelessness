#pragma once
#include "../xlive/xsocket.hpp"
#include <vector>

bool InitXllnNetwork();
bool UninitXllnNetwork();

namespace XllnNetworkPacket {
	const char* const TYPE_NAMES[] {
		"UNKNOWN"
		, "TITLE_PACKET"
		, "TITLE_BROADCAST_PACKET"
		, "PACKET_FORWARDED"
		, "UNKNOWN_USER_ASK"
		, "UNKNOWN_USER_REPLY"
		, "CUSTOM_OTHER"
		, "LIVE_OVER_LAN_ADVERTISE"
		, "LIVE_OVER_LAN_UNADVERTISE"
		, "HUB_REQUEST"
		, "HUB_REPLY"
		, "QOS_REQUEST"
		, "QOS_RESPONSE"
		, "HUB_OUT_OF_BAND"
		, "HUB_RELAY"
		, "DIRECT_IP_REQUEST"
		, "DIRECT_IP_RESPONSE"
	};
	typedef enum : uint8_t {
		XLLN_NPT_UNKNOWN = 0
		, XLLN_NPT_FIRST
		
		, XLLN_NPT_TITLE_PACKET = XLLN_NPT_FIRST
		, XLLN_NPT_TITLE_BROADCAST_PACKET // Unused from XLLN v1.6.
		, XLLN_NPT_PACKET_FORWARDED
		, XLLN_NPT_UNKNOWN_USER_ASK // Unused from XLLN v1.6.
		, XLLN_NPT_UNKNOWN_USER_REPLY // Unused from XLLN v1.6.
		, XLLN_NPT_CUSTOM_OTHER // Unused.
		, XLLN_NPT_LIVE_OVER_LAN_ADVERTISE
		, XLLN_NPT_LIVE_OVER_LAN_UNADVERTISE
		, XLLN_NPT_HUB_REQUEST
		, XLLN_NPT_HUB_REPLY
		, XLLN_NPT_QOS_REQUEST
		, XLLN_NPT_QOS_RESPONSE
		, XLLN_NPT_HUB_OUT_OF_BAND // Unused from XLLN v1.6.
		, XLLN_NPT_HUB_RELAY // Unused from XLLN v1.6.
		, XLLN_NPT_DIRECT_IP_REQUEST
		, XLLN_NPT_DIRECT_IP_RESPONSE
		
		, XLLN_NPT_LAST = XLLN_NPT_DIRECT_IP_RESPONSE
	} TYPE;
	
	const char* GetPacketTypeName(XllnNetworkPacket::TYPE type);
	
#pragma pack(push, 1) // Save then set byte alignment setting.
	
	typedef struct _TITLE_PACKET {
		uint32_t instanceId = 0;
		uint16_t titlePortSource = 0; // Host Byte Order.
		uint16_t titlePortDestination = 0; // Host Byte Order.
	} TITLE_PACKET;
	
	typedef struct _PACKET_FORWARDED {
		SOCKADDR_STORAGE originSockAddr;
		// The data following this is the forwarded packet data.
	} PACKET_FORWARDED;
	
	typedef struct _HUB_REQUEST_PACKET {
		uint32_t xllnVersion = 0; // version of the requester.
		uint32_t instanceId = 0; // Instance ID of the requester.
		uint32_t titleId = 0;
		uint32_t titleVersion = 0;
		uint32_t portBaseHBO = 0;
	} HUB_REQUEST_PACKET;
	
	typedef struct _HUB_REPLY_PACKET {
		uint8_t isHubServer = 0; // boolean.
		uint32_t xllnVersion = 0; // version of the replier.
		uint32_t recommendedInstanceId = 0; // the Instance ID that should be used instead (in case of collisions).
	} HUB_REPLY_PACKET;
	
	typedef struct _LIVE_OVER_LAN_UNADVERTISE {
		uint32_t instanceId = 0;
		uint8_t sessionType = 0;
		XUID xuid = 0;
	} LIVE_OVER_LAN_UNADVERTISE;
	
	typedef struct _QOS_REQUEST {
		uint32_t qosLookupId = 0;
		uint64_t sessionId = 0; // XNKID
		uint32_t probeId = 0;
		uint32_t instanceId = 0; // Instance ID of the requester.
	} QOS_REQUEST;
	
	typedef struct _QOS_RESPONSE {
		uint32_t qosLookupId = 0;
		uint64_t sessionId = 0; // XNKID
		uint32_t probeId = 0;
		uint32_t instanceId = 0; // Instance ID of the responder.
		uint8_t enabled = 0;
		uint16_t sizeData = 0; // the amount of data appended to the end of this packet type.
	} QOS_RESPONSE;
	
	typedef struct _DIRECT_IP_REQUEST {
		uint32_t joinRequestSignature = 0;
		uint8_t passwordSha256[32];
	} DIRECT_IP_REQUEST;
	
	typedef struct _DIRECT_IP_RESPONSE {
		uint32_t joinRequestSignature = 0;
		uint32_t instanceId = 0;
		uint32_t titleId = 0;
	} DIRECT_IP_RESPONSE;
	
#pragma pack(pop) // Return to original alignment setting.
}

namespace XllnNetworkBroadcastEntity {
	const char* const TYPE_NAMES[] {
		"UNKNOWN",
		"BROADCAST_ADDR",
		"HUB_SERVER",
		"OTHER_CLIENT",
	};
	typedef enum : uint8_t {
		XLLN_NBE_UNKNOWN = 0,
		XLLN_NBE_BROADCAST_ADDR,
		XLLN_NBE_HUB_SERVER,
		XLLN_NBE_OTHER_CLIENT,
	} TYPE;
	
	typedef struct _BROADCAST_ENTITY {
		SOCKADDR_STORAGE sockaddr;
		__time64_t lastComm = 0;
		TYPE entityType = TYPE::XLLN_NBE_UNKNOWN;
		uint32_t xllnVersion = 0;
	} BROADCAST_ENTITY;
}

typedef struct _XLLN_NET_SEND_PACKET_INFO {
	// The data to send. The data must be wrapped in XllnNetworkPacket::TYPE.
	uint8_t* data = 0;
	size_t dataSize = 0;
	// If set, when data is sent this is used to notifiy the Title.
	SOCKET sourceTitleSocketHandle = INVALID_SOCKET;
	uint32_t titlePayloadSize = 0;
	// If this is 0 then use destinationAddress instead.
	uint32_t destinationInstanceId = 0;
	SOCKADDR_STORAGE destinationAddress = {AF_UNSPEC};
	
	~_XLLN_NET_SEND_PACKET_INFO()
	{
		if (data) {
			delete[] data;
			data = 0;
		}
	}
} XLLN_NET_SEND_PACKET_INFO;

extern CRITICAL_SECTION xlln_critsec_network_send;
extern std::vector<XLLN_NET_SEND_PACKET_INFO*> xlln_network_send_queue;

extern uint16_t xlln_network_instance_base_port;
extern uint16_t xlln_network_instance_port;
extern HANDLE xlln_network_instance_port_mutex;

extern CRITICAL_SECTION xlln_critsec_network_broadcast_addresses;
extern std::vector<XllnNetworkBroadcastEntity::BROADCAST_ENTITY> xlln_network_broadcast_addresses;

extern CRITICAL_SECTION xlln_critsec_network_net_entity;
extern std::map<uint32_t, SOCKADDR_STORAGE> xlln_net_entity_instance_id_to_remote_address;

bool XllnNetEntityGetXnaddrXnkidByInstanceId(const uint32_t instanceId, XNADDR* xnaddr, XNKID* xnkid);
bool SendPacketToRemoteInstance(XLLN_NET_SEND_PACKET_INFO* send_packet);
bool SubmitDataToTitleSocket_(XLIVE_TITLE_SOCKET* title_socket, XTS_RECV_PACKET* recv_packet);
bool SubmitDataToTitleSocketHandle(SOCKET title_socket_handle, XTS_RECV_PACKET* recv_packet);
bool SubmitDataToTitleSocketHandle_(SOCKET title_socket_handle, XTS_RECV_PACKET* recv_packet);
